{% include "Includes/Header.stencil" %}

import Foundation
import XCTest

final class APITests {
  private let client: APIClient

  init() {
    self.client = APIClient.init(baseURL: "dzen.ru")
  }

  private func testListPetsRequest() {
    let request = Petshop.Pets.ListPets.Request.init()
    client.makeRequest(request) { _ in
      XCTAssertNotNil(request.success)
      XCTAssertTrue(request.successful)
    }
  }

  private func testCreatePetsRequest() {
    let request = Petshop.Pets.CreatePets.Request.init()
    client.makeRequest(request) { _ in
      XCTAssertNotNil(request.success)
      XCTAssertTrue(request.successful)
    }
  }

  private func testShowPetByIdRequest() {
    let request = Petshop.Pets.ShowPetById.Request.init(petId: "1")
    client.makeRequest(request) { _ in
      XCTAssertNotNil(request.success)
      XCTAssertTrue(request.successful)
    }
  }

  private func testShowPetByIdRequest() {
    let request = Petshop.Pets.UpdatePetWithForm.Request.init(petId: "1", name: "newName")
    client.makeRequest(request) { _ in
      XCTAssertNotNil(request.success)
      XCTAssertTrue(request.successful)
    }
  }
}
