import Foundation
import PathKit
import DGenKit
import SwiftCLI

let version = "1.0.0"
let generateCommand = GenerateCommand()
let cli = CLI(name: "dgen", version: version, description: "API code generator", commands: [generateCommand])
cli.goAndExit()
